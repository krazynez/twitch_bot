aiohttp>=3.7.3
twitchio>=1.2.1
websockets>=8.1
requests>=2.25.1
bs4>=0.0.1
python-weather>=0.3.7
names>=0.3.0
selenium>=4.2.0
