#!/home/brennen/.local/share/virtualenvs/temp-EkqHcIJs/bin/python

import requests
from bs4 import BeautifulSoup as bs
class Chimay_check:
    def __init__(self):
        self.URL = 'https://www.lcbo.com/webapp/wcs/stores/servlet/SearchDisplay?categoryId=&storeId=10203&catalogId=10051&langId=-1&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q&pageView=&beginIndex=0&pageSize=12&searchTerm=chimay&authToken=-1002%252CetYHJunN2fPHnqWYb6iXk2V4thGGKMCP7yRvypmsoac%253D'

        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0'
            }

        self.cookies = {
            'Cookie': 'JSESSIONID=0000e_ETqz65iRDd_rI4yE7oSPM:ecomapp06; WC_PERSISTENT=EFHfn8TFwCzcHD1gmBhXPozmz4rVgs9Ar0mUBstn48w%3D%3B2021-02-26+13%3A25%3A37.067_1614363937065-76256_10203_-1002%2C-1%2CCAD%2CuMgWIyPqf%2Bd00knNo3CXNeewGuwg3KU%2BLhdASuX8NkWo2TeC%2Fs2LNRl5lyNOYQelPhdjtW%2BEpJJwdTBdpLzkZA%3D%3D_10203; WC_SESSION_ESTABLISHED=true; WC_AUTHENTICATION_-1002=-1002%2CetYHJunN2fPHnqWYb6iXk2V4thGGKMCP7yRvypmsoac%3D; WC_ACTIVEPOINTER=-1%2C10203; WC_USERACTIVITY_-1002=-1002%2C10203%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2C1617095574%2C8AAETmncUS8cSO6ISwLj1uB0xJDJUbvO6ME%2FC%2BWKUtyd2TPAD%2BKwiSTS7chwofEid%2Bb%2FViFerSm68wj1fwteR7wENt1YmZojg6tw9JHwYX5y8msgUXpAtt38SZmRaW7bS3BoVWpUmZyiecPZ7kGZbxbqoBklsTByvX9L9wTcjD9lEt4pEnnCnCpJku1xzvkVn7HqUKH%2Bm3wzvimvz1dxTxu%2BztCeV1%2BjKT2SESE7KdU107ywqoM9pPyCYjSXdS%2Fc; WC_GENERIC_ACTIVITYDATA=[4438383276%3Atrue%3Afalse%3A0%3ADF8SqJSUInYGsD07i2CFMqpwb5coZUWglexJuzcFS04%3D][com.ibm.commerce.context.ExternalCartContext|null][com.ibm.commerce.context.entitlement.EntitlementContext|10502%2610502%26null%26-2000%26null%26null%26null][com.ibm.commerce.store.facade.server.context.StoreGeoCodeContext|null%26null%26null%26null%26null%26null][com.ibm.commerce.catalog.businesscontext.CatalogContext|10051%26null%26false%26false%26false][CTXSETNAME|Store][com.ibm.commerce.context.base.BaseContext|10203%26-1002%26-1002%26-1][com.ibm.commerce.context.audit.AuditContext|1614363937065-76256][com.lcbo.lco.commerce.context.LCOContext|false][com.ibm.commerce.context.experiment.ExperimentContext|null][com.ibm.commerce.giftcenter.context.GiftCenterContext|null%26null%26null][com.ibm.commerce.context.globalization.GlobalizationContext|-1%26CAD%26-1%26CAD]; priceMode=1; QueueITAccepted-SDFrts345E-V3_2020pandemic=EventId%3D2020pandemic%26QueueId%3De5fad1b2-9cd3-40ea-b947-94a5aed7f9c9%26RedirectType%3Dsafetynet%26IssueTime%3D1614363938%26Hash%3D435c423997243368bf82aeaf23689156d4d4dc067cd762718fab6e7f7a6bf329; CompareItems_10203=; searchTermHistory=%7Cchimay%7Cchimay; kampyle_userid=d29c-859b-68b1-ae26-e642-3bd7-e4c1-e4e4; kampyleUserSession=1614363939408; kampyleSessionPageCounter=1; kampyleUserSessionsCount=1; WC_CartOrderId_10203=; __z_a=1536862170337039071733703; languagepopupshown=true; lang=en; WC_stCity=N3S%205S8; storetype=clickcollect; WC_physicalStoreAddress=MARKET%20S%20%26%20ICOMM; ffmcenter-id=44; WC_physicalStores=715841602; WC_physicalCity=BRANTFORD; __zjc4671=5071674302; WC_DeleteCartCookie_10203=true'
            }

        self.page = requests.get(self.URL, headers=self.headers, cookies=self.cookies)

        self.soup = bs(self.page.content, 'html.parser')

        #results = soup.find_all('div', itemprop='name')
        self.results = self.soup.find_all('div', class_='productListingWidget')

        #for r in results:
        #    print(r)

        avaiable = self.soup.find_all('div', class_='row product')

        for a in avaiable:
            for b in a.find_all('p', 'inventoryOption'):
                if not '(Unavailable)' in b.text and not 'Deliver' in b.text: 
                    id=b.parent.get('id')

        j=[f for f in id if f.isdigit()]
        j=''.join(j)

        chimays = self.soup.find_all('a', id=f'CatalogEntryViewDetailsLink_{j}')

        chimay_list = []
        for c in chimays:
            chimay_list.append(c.text)

        for c in chimay_list:
            ch = ''.join(c)
            

        self.chimay = ch

def chimay():
    return Chimay_check()
