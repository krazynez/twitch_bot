#!/usr/bin/env python3
from comms import bot
import events

if __name__ == '__main__':
    try:
        bot.run() 
    except Exception as e:
        print(f'Error: {e}')
