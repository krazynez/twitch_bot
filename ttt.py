import sys
class TickTacToe:
    def __init__(self):
        self.gameboard = [ 
                ['_', '_', '_'],
                ['_', '_', '_'],
                ['_', '_', '_']
                ]
        self.players = {'X': None, 'O': None}
        self.first_run = True
        self.winner = False
        self.count = 0
        self.players_turn = self.players['X']
        self.row = 0
        self.counter = 0

    def players_t(self):
        return self.players_turn

    def __del__(self):
        return "Game has been deleted"

    def setup(self, player) -> str:
        if self.first_run is True:
            if self.players['X'] is None:
                self.players['X'] = player
                self.first_run = False
                return 'X'
        if not self.players['X'] in player and self.players['O'] is None and self.first_run is False:
                self.players['O'] = player
                return 'O'
        else:
            return 'Game already running'
        self.first_run = False
    
    def turn(self) -> str:
        if self.count % 2 != 0:
            self.count += 1
            self.players_turn = self.players['X']
            return f"{self.players['X']}'s turn"
        else:
            self.count += 1
            self.players_turn = self.players['O']
            return f"{self.players['O']}'s turn"

    def counter(self):
        if self.counter % 2 == 0:
            self.counter += 1
            type(self.counter)
            return self.counter
        else:
            type(self.counter)
            return self.counter

    # Need to figure out how to send one row at a time to the ctx (async?)
    def board(self, row):
        return f"Row {row+1}", ' '.join(self.gameboard[row])
            
        self.row += 1
    def board_layout(self, row):
        grid_layout = [ 
                ['| 0,0 |', '| 0,1 |', '| 0,2 |'],
                ['| 1,0 |', '| 1,1 |', '| 1,2 |'],
                ['| 2,0 |', '| 2,1 |', '| 2,2 |']
                ]
        return ' '.join(grid_layout[row])

    def play(self, x=None, y=None):
        x=int(x)
        y=int(y)
        if x > 2 or x < 0 and y > 2 or y < 0:
            return 'Err: Stop trying to break my game you fool! Now you must reset the game good job.'
        elif self.gameboard[x][y] == '_':
            a = list(self.players.keys())
            if self.count % 2 != 0:
                self.gameboard[x][y] = a[1]
                win = self.winner_check()
                turn = self.turn()
                if win is not None:
                    return win
                else:
                    return turn
            else:
                self.gameboard[x][y] = a[0]
                win = self.winner_check()
                turn = self.turn()
                if win is not None:
                    return win
                else:
                    return turn
        else:
            return 'Position already taken'

    def reset(self):
        self.__init__()
        exit()

    ###################
    # (0,0) (0, 1) (0, 2)
    # (1,0) (1, 1) (1, 2)
    # (2,0) (2, 1) (2, 2)
    ###################

    def winner_check(self) -> str:
        if self.winner == True:
            self._init__()
            return "Game is over type: #ttt to start a new one."
        # Top row (0, 0 -> 0, 2)
        if self.gameboard[0][0] == 'X' and self.gameboard[0][1] == 'X' and self.gameboard[0][2] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[0][0]]} won!"
        elif self.gameboard[0][0] == 'O' and self.gameboard[0][1] == 'O' and self.gameboard[0][2] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[0][0]]} won!"
        # Top Middle Down  0, 1 -> 2, 1
        elif self.gameboard[0][1] == 'X' and self.gameboard[1][1] == 'X' and self.gameboard[2][1] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[0][1]]} won!"
        elif self.gameboard[0][1] == 'O' and self.gameboard[1][1] == 'O' and self.gameboard[2][1] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[0][1]]} won!"
        # Top Right Down  0, 2 -> 2, 2
        elif self.gameboard[0][2] == 'X' and self.gameboard[1][2] == 'X' and self.gameboard[2][2] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[0][2]]} won!"
        elif self.gameboard[0][2] == 'O' and self.gameboard[1][2] == 'O' and self.gameboard[2][2] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[0][2]]} won!"
        # Top Left Down 0,0 -> 2,0
        elif self.gameboard[0][0] == 'X' and self.gameboard[1][0] == 'X' and self.gameboard[2][0] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[0][0]]} won!"
        elif self.gameboard[0][0] == 'O' and self.gameboard[1][0] == 'O' and self.gameboard[2][0] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[0][0]]} won!"
        # Middle row  1, 0 -> 1, 2
        elif self.gameboard[1][0] == 'X' and self.gameboard[1][1] == 'X' and self.gameboard[1][2] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[1][0]]} won!"
        elif self.gameboard[1][0] == 'O' and self.gameboard[1][1] == 'O' and self.gameboard[1][2] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[1][0]]} won!"
        # Bottom row  2, 0 -> 2, 2
        elif self.gameboard[2][0] == 'X' and self.gameboard[2][1] == 'X' and self.gameboard[2][2] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[2][0]]} won!"
        elif self.gameboard[2][0] == 'O' and self.gameboard[2][1] == 'O' and self.gameboard[2][2] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[2][0]]} won!"
        # Left Diagonal  0, 0 -> 2, 0
        elif self.gameboard[0][0] == 'X' and self.gameboard[1][1] == 'X' and self.gameboard[2][2] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[0][0]]} won!"
        elif self.gameboard[0][0] == 'O' and self.gameboard[1][1] == 'O' and self.gameboard[2][2] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[0][0]]} won!"
        # Right Diagonal  0, 2 -> 2, 0
        elif self.gameboard[0][2] == 'X' and self.gameboard[1][1] == 'X' and self.gameboard[2][0] == 'X':
            self.winner = True
            return f"{self.players[self.gameboard[0][2]]} won!"
        elif self.gameboard[0][2] == 'O' and self.gameboard[1][1] == 'O' and self.gameboard[2][0] == 'O':
            self.winner = True
            return f"{self.players[self.gameboard[0][2]]} won!"
        else:
            return
