## Prefix

`#`

## Commands

1. `ping`

2. `psa` (lastmiles)

3. `it_crowd`

4. `note_to_self`

5. `notes`

6. `beer` (lastmiles)

7. `canadian` (lastmiles)

8. `rgb` (lastmiles)

9. `timer` (syntax: #timer 1m)

10. `link`

11. `distraction` (mitchsworkshop)

12. `add_distraction` (mitchsworkshop)

13. `vim` (mitchsworkshop)

14. `python`

15. `source`

16. `rps` (rock, paper, scissors)

17. `shed` (lastmiles)

18. `convert`

19. `help`

20. `sudo`

21. `ttt` (Tic Tac Toe)

22. `chimay` (lastmiles)
