#!/usr/bin/env python3
import os,time,random,requests, names
from twitchio.ext import commands
from chimay_checker import Chimay_check
from ttt import TickTacToe
from florida_man import Florida_Man
from twitch_viewer_count import  Twitch_Viewer_Count
from datetime import datetime
# Setting up bot
bot = commands.Bot(
        token=os.environ['TMI_TOKEN'],
        # ARM/AARCH64 drops irc_ prefix
        # token=os.environ['TMI_TOKEN'],
        client_id=os.environ['CLIENT_ID'],
        nick=os.environ['BOT_NICK'],
        prefix=os.environ['BOT_PREFIX'],
        initial_channels=[os.environ['CHANNEL']]

)
####################### COMMANDS ##################################

print('Channel:', os.environ['CHANNEL'])

# Ping the bot
@bot.command()
async def ping(ctx):
    await ctx.channel.send('pong!')

# Lastmiles PSA
@bot.command()
async def psa(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send('https://clips.twitch.tv/AgitatedTastyFlamingoSquadGoals-NZUootjJP-9E9Ruu')
    else:
        await ctx.channel.send('https://www.twitch.tv/lastmiles/clip/AltruisticPlayfulBeanHeyGirl')

@bot.command()
async def psa_backup(ctx):
    await ctx.channel.send('https://web.archive.org/web/20210807130533/https://production.assets.clips.twitchcdn.net/AT-cm%7C981875982.mp4?sig=8bb42d9e877a6c08a106fdc12bb610fe12306eb1&token=%7B%22authorization%22%3A%7B%22forbidden%22%3Afalse%2C%22reason%22%3A%22%22%7D%2C%22clip_uri%22%3A%22https%3A%2F%2Fproduction.assets.clips.twitchcdn.net%2FAT-cm%257C981875982.mp4%22%2C%22device_id%22%3A%225131d400ff8c0bee%22%2C%22expires%22%3A1628413494%2C%22user_id%22%3A%2270428043%22%2C%22version%22%3A2%7D')

# IT Crowd number for 999
@bot.command()
async def it_crowd(ctx):
    await ctx.channel.send('0118 999 881 99 9119 7253')

# Add notes
@bot.command()
async def note_to_self(ctx):
    import os
    if not os.path.isfile(f'./db/{ctx.channel.name}.txt'): 
        open(f'./db/{ctx.channel.name}.txt', 'a').close()
    with open(f'./db/{ctx.channel.name}.txt', 'a') as j:
        contents = ' '.join(ctx.message.content.lower().split()[1:])
        j.write(contents)
        j.write('\n')
        j.close()
    await ctx.channel.send('Note added.')

# Get notes
@bot.command()
async def notes(ctx):
    import os
    if os.path.isfile(f'./db/{ctx.channel.name}.txt'): 
        with open(f'./db/{ctx.channel.name}.txt', 'r') as f:
            n = f.readlines()
            f.close()
        a=[]
        for note in n:
            a.append(note)
        for i in range(len(a)):
            await ctx.channel.send(f"Item {i+1} of {len(a)}=> {a[i]}")
            time.sleep(2)
    else:
        await ctx.channel.send(f'{ctx.author.name} there are no notes for this channel yet.')

# Beer Money
@bot.command()
async def beer(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send('https://clips.twitch.tv/AntediluvianAmusedTrayPJSalt-wrKYlZepuG_JKU4e')
    else:
        await ctx.channel.send(f"{ctx.author.name} chug chug as fast as you can you can't out drink me I am the beerman")

@bot.command()
async def paste(ctx):
    await ctx.channel.send('Use this for code snippets: https://paste.debina.net use the plain text link afterwards.')

@bot.command()
async def beertime(ctx):
    if ctx.channel.name == "lastmiles":
        await ctx.channel.send("Home study kit time. Go grab your beers!")

@bot.command()
async def chimay(ctx):
    if ctx.channel.name == 'lastmiles' or ctx.channel.name == 'krazynez_2':
        chimay_count = Chimay_check()
        await ctx.channel.send(f'Avaliable Chimay at your store Dennis: {chimay_count.chimay}')

# Pay me
@bot.command()
async def payme(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send('In the lovely words of Dennis "Pay me bitch."')

# Stapler
@bot.command()
async def stapler(ctx):
    await ctx.channel.send("If you don't have a Red Stapler, go get one now. It is required.")

# Canada
@bot.command()
async def canadian(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send("I'm sorry")

# RGB quote
@bot.command()
async def rgb(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send("RGB is for snowflakes.")

@bot.command()
async def bedtime(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send(f"Bedtime Story for Larry: https://clips.twitch.tv/AgitatedTentativePotBIRB-_6vSohDKzCvr2rDt")

# Timer
@bot.command()
async def timer(ctx):
    timer=0
    s = ctx.message.content
    _time = s.split(' ')[2]
    if _time.lower() == 'm':
        timer = s.split(' ')[1]
        count = int(timer)
        times_up = count * 60
        ctx.channel.send(f'{ctx.author.name} your timer is set')
        while times_up > 0:
            times_up = times_up - 1
            time.sleep(1)
        await ctx.channel.send(f'{ctx.author.name} your timer went off!!!')
# Link save
@bot.command()
async def link(ctx):
    if len(ctx.message.content.split()) == 1:
        with open('./db/links.txt', 'r') as read_f:
            l = read_f.readlines()
            read_f.close()
        _link = ''.join(l) 
        await ctx.channel.send(f'Link => {_link}')

    elif ctx.message.content.lower().split()[1] == 'add':
        with open('./db/links.txt', 'w') as f:
            f.write(ctx.message.content.split()[2])
            f.close()
        await ctx.channel.send('link added.')

# Distraction counter for Mitch
@bot.command()
async def distract(ctx):
    if ctx.channel.name == 'mitchsworkshop' or ctx.channel.name == 'krazynez_2':
        with open('./db/mitchsworkshop_distraction.txt', 'r') as f:
            distract = f.read()
            f.close()
        await ctx.channel.send(f'Mitch has been distracted: {distract} times!')

@bot.command()
async def add_distraction(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        if not os.path.isfile('./db/mitchsworkshop_distraction.txt'):
            with open('./db/mitchsworkshop_distraction.txt', 'w') as f:
                f.write('0')
                f.close()
        with open('./db/mitchsworkshop_distraction.txt', 'r') as fi:
            current = fi.read()
            fi.close()
        if current is None:
            current = 0
        current_int = int(current)+1
        current = current.replace(current, str(current_int))

        with open('./db/mitchsworkshop_distraction.txt', 'w') as fil:
            fil.write(current)
            fil.close()
        await ctx.channel.send('distraction +1')

@bot.command()
async def os(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send('Pop!_OS')
    elif ctx.channel.name == 'lastmiles':
        await ctx.channel.send('Probably Devuan, Debian, FreeBSD, Solaris, or RISC-V, if you are talking about desktop enviroment probably LXDE, WM is probably OpenBox.')

@bot.command()
async def nvidia(ctx):
    await ctx.channel.send('https://images-cdn.9gag.com/photo/5781895_700b.jpg')

@bot.command()
async def sh(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send("https://git.sr.ht/~blastwave/bw")

@bot.command()
async def discord(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send('Ask for programming help or hang out with other nerds on Discord! https://discord.gg/vF6W2bdKFH ( His Bot is probably down that is why I made this for a backup ;-p )')
   
@bot.command()
async def flyboy(ctx):
    await ctx.channel.send('Just remember kids, Flyboy is always correct. Mitch is probably wrong.')

#@bot.command()
#async def vim(ctx):
#    if ctx.channel.name == 'mitchsworkshop':
#        await ctx.channel.send('Today will will learn VIM, Don\'t let Mitch tell you otherwise')

@bot.command()
async def judged(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send('You have been JUDGED! Mitch!')

@bot.command()
async def caps(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send('If you search it has to be in all CAPS OKAY!')

@bot.command()
async def betterBot(ctx):
    bots = ['krazybot_2', 'MitchsRobot', 'botsenth545']
    random.shuffle(bots)
    await ctx.channel.send(f'This is the bot for the day: {random.choice(bots)}')

@bot.command()
async def calc(ctx):
    b=[]
    d=True
    with_decimal=False
    c=0.0
    stripped_values = ctx.message.content.split()
    if stripped_values[1].lower().endswith('k'):
        decimal = stripped_values[1]
        dec = decimal.split('k')[0]
        stripped_values = ' '.join(map(str, stripped_values))
        stripped_values = ' '.join(stripped_values.replace(decimal, '').rsplit())
        with_decimal = True
        for i in range(1, len(stripped_values)):
            if stripped_values[i].isdigit() or stripped_values[i].replace('.', '').isdigit():
                b.append(float(stripped_values[i]))
    else:
       for i in range(1, len(ctx.message.content.split())):
            if ctx.message.content.split()[i].isdigit() or ctx.message.content.split()[i].replace('.', '').isdigit():
                b.append(float(ctx.message.content.split()[i])) 
#    if '+' or '/' or '-' or '*' in ctx.message.content.split()[1:-2]:
#        for j in range(len(b)):
#            if ctx.message.content.split()[-2] == '+':
#                c += b[j]
#            elif ctx.message.content.split()[-2] == '/':
    if ctx.message.content.split()[-1] == '+':
        for j in range(len(b)):
            c += b[j]
    if ctx.message.content.split()[-1] == '-':
        first = True
        c = 0.0
        for j in reversed(range(len(b))):
            if first:
                c = b[j]
                first = False
            else:
                c -= b[j]
    if ctx.message.content.split()[-1] == '*':
        for j in range(len(b)):
            c *= b[j]
    if ctx.message.content.split()[-1] == '/':
        for j in reversed(range(len(b))):
            if ctx.message.content.split()[-2] == '0':
                await ctx.channel.send(f'{ctx.author.name} stop dividing by 0!')
                return
            if d is True:
                c = b[j]
                d = False
            else:
                c /= b[j]
    if ctx.message.content.split()[-1] == '*':
        for j in range(len(b)):
            if d is True:
                c = float(b[j])
                d = False
            else:
                c *= float(b[j])
    if with_decimal:
        await ctx.channel.send(f"Result: {c:.4f}. Currently only does 4 decimals places for now.")
        return
    else:
        await ctx.channel.send(f"Result: {c}")
        return


@bot.command()
async def vscode(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send("VSCode stands for Vim Should Code, I don't know what vscode you're talking about")

@bot.command()
async def python(ctx):
    await ctx.channel.send("We are the knights who say 'Ni!'")

@bot.command()
async def sfd(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send('Shit, Fuck, Damn!')

@bot.command()
async def src(ctx):
    await ctx.channel.send("Here is my shitty source code: https://gitlab.com/krazynez/twitch_bot")

# Chemical shed
@bot.command()
async def shed(ctx):
    if ctx.channel.name == 'lastmiles' or ctx.channel.name == 'krazynez_2':
        word = ctx.message.content.lower().split()[1]
        await ctx.channel.send(f'{word} should be taken out back to the chemical shed')

@bot.command()
async def sudo(ctx):
    if ctx.channel.name.lower() == 'mitchsworkshop':
        await ctx.channel.send('sudo apt purge mitch && sudo apt install wifesworkshop')
    else:
        await ctx.channel.send("sh: sudo: command not found...")

@bot.command()
async def offended(ctx):
    await ctx.channel.send("Awww your offended? You must be a snowflake then. Go away")

# Convert F to C or C to F ; kg to lb, lb to kg
@bot.command()
async def convert(ctx):
    if ctx.message.content.lower().split()[2] == 'f':
        f = ctx.message.content.lower().split()[1]
        f = int(f)
        c = (f - 32) * (5 / 9)
        await ctx.channel.send(f'{ctx.author.name} your conversion is: {c:.2f}°C')
    if ctx.message.content.lower().split()[2] == 'c':
        c = ctx.message.content.lower().split()[1]
        c = int(c)
        f = (c * 1.8) + 32
        await ctx.channel.send(f'{ctx.author.name} your conversion is: {f:.2f}°F')
    
    # kg -> lb ; lb -> kg
    if ctx.message.content.lower().split()[2] == 'kg':
        k = ctx.message.content.lower().split()[1]
        k = int(k)
        p = (k / 0.45359237)
        await ctx.channel.send(f'{ctx.author.name} your conversion is: {p:.2f} lbs')
    if ctx.message.content.lower().split()[2] == 'lbs':
        p = ctx.message.content.lower().split()[1]
        p = int(p)
        k = (p * 0.45359237)
        await ctx.channel.send(f'{ctx.author.name} your conversion is: {k:.2f} kg')

@bot.command()
async def wwad(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send('Probably something better than @MitchsWorkshop would do.')

@bot.command()
async def mitch(ctx):
    await ctx.channel.send('Mitch ? Don\'t know that guy, maybe you are looking for Absenth, Flyboy, Anders, Masterfirewall or Krazynez?')

@bot.command()
async def vim(ctx):
    if len(ctx.message.content.lower().split()) < 2:
        await ctx.channel.send('Fun fact VIM and krazynez_2 have the same birthday!')
    elif ''.join(ctx.message.content.lower().split()[1:]) == 'ct':
        await ctx.channel.send('Fun fact, if you do `ct<char until to change>` it will will delete everything from your cursor until the char you said, and be in insert mode afterwards')
    elif ' '.join(ctx.message.content.lower().split()[1:]) == 'empty space' or ''.join(ctx.message.content.lower().split()[1:]) == ':g/^$/d' or ' '.join(ctx.message.content.lower().split()[1:]) == 'delete empty line':
        await ctx.channel.send('Fun fact, if you type :g/^$/d it will delete all empty lines in the buffer')
    elif ''.join(ctx.message.content.lower().split()[1:]) == 'macro' or ''.join(ctx.message.content.lower().split()[1:]) == 'macros':
        await ctx.channel.send("Fun fact, if you do something very repetitive you can use a macro to rerun commands: qp iprint('<press <ESC> then q again>') ; then you can just @r to create another print('')")


@bot.command()
async def set_topic(ctx):
    import os
    if not os.path.isfile('./db/topic.txt'):
        open('./db/topic.txt', 'a').close()
    topic = ' '.join(ctx.message.content.lower().split()[1:])
    with open('./db/topic.txt', 'w') as f:
        f.write(topic)
        f.close()
    await ctx.channel.send(f'Topic Changed')

@bot.command()
async def topic(ctx):
    with open('./db/topic.txt', 'r') as f:
        topic = f.read()
        f.close()
    await ctx.channel.send(f'Topic is: {topic}')

@bot.command()
async def countdown(ctx):
    if ctx.channel.name == "lastmiles" or ctx.channel.name == "krazynez_2":
        for i in reversed(range(0, 3)):
            await ctx.channel.send(f"{i+1}")
            time.sleep(3)
        await ctx.channel.send('Boom!')

@bot.command()
async def wat(ctx):
    await ctx.channel.send('https://www.destroyallsoftware.com/talks/wat')

@bot.command()
async def linux(ctx):
    if len(ctx.message.content.split()) < 2:
        await ctx.channel.send("I'd just like to interject for a moment. What you're referring to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without realizing it. Through a ...")

        # Twitch only allows 500 chars max
        #await ctx.channel.send("I'd just like to interject for a moment. What you're referring to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called \"Linux\", and many of its users are not aware that it is basically the GNU system, developed by the GNU Project. There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine's resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called \"Linux\" distributions are really distributions of GNU/Linux.")
    else:
        await ctx.channel.send(f"I'd just like to interject for a moment. What you're referring to as {ctx.message.content.split()[1]}, is in fact, GNU/{ctx.message.content.split()[1]}, or as I've recently taken to calling it, GNU plus {ctx.message.content.split()[1]}. {ctx.message.content.split()[1]} is not an operating system unto itself, but rather an other free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without ...")

        # Twitch only allows 500 chars max
        #await ctx.channel.send(f"I'd just like to interject for a moment. What you're referring to as {ctx.message.content.split()[1]}, is in fact, GNU/{ctx.message.content.split()[1]}, or as I've recently taken to calling it, GNU plus {ctx.message.content.split()[1]}. {ctx.message.content.split()[1]} is not an operating system unto itself,     but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called \"{ctx.message.content.split()[1]}\", and many of its users are not aware that it is basically the GNU system, developed by the GNU Project. There really is a {ctx.message.content.split()[1]}, and these people are using it, but it is just a part of the system they use. {ctx.message.content.split()[1]} is the kernel: the program in the system that allocates the machine's resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. {ctx.message.content.split()[1]} is normally used in combination with the GNU operating system: the whole system is basically GNU with {ctx.message.content.split()[1]} added, or GNU/{ctx.message.content.split()[1]}. All the so-called \"{ctx.message.content.split()[1]}\" distributions are really distributions of GNU/{ctx.message.content.split()[1]}.")

@bot.command()
async def bs(ctx):
    req = requests.get("https://corporatebs-generator.sameerkumar.website/").content.decode().split(":")[1].replace("}", '').replace('"', '')
    await ctx.channel.send(req)

@bot.command()
async def whiskeyVsScotch(ctx):
    if ctx.channel.name == 'lastmiles':
        await ctx.channel.send('https://clips.twitch.tv/TemperedGlutenFreeHabaneroShazBotstix-Lk9qGctPJzBoni4V')

@bot.command()
async def software(ctx):
    await ctx.channel.send('https://www.twitch.tv/lastmiles/clip/FrigidStrongElephantTwitchRPG-WwjLOTTcR9l0xjQ1')

@bot.command()
async def times(ctx):
    import random
    a = random.randint(1000, 100000)
    await ctx.channel.send(f'You got messed up {a} times so far LUL')

@bot.command()
async def jebaited(ctx):
    import os
    if not os.path.isfile('./db/jebaited.txt'):
        with open('./db/jebaited.txt', 'w') as f:
            f.write('0')
            f.close()

    with open('./db/jebaited.txt', 'r+') as input_file:
            number = input_file.read()
            input_file.seek(0)
            input_file.truncate()
            number = int(number.rstrip())+1
            input_file.write(str(number))
            input_file.close()
    if len(ctx.message.content.lower().split()) < 2:
        await ctx.channel.send(f'{ctx.author.name} has added to the jabaited coutner, {number} is now the count')
    else:
        await ctx.channel.send(f'{ctx.message.content.split()[1]} has added to the jabaited coutner, {number} is now the count')

@bot.command()
async def help(ctx):
    if ctx.message.content == "#help" :
        await ctx.channel.send(f'Commands: convert, rps, whiskeyVsScotch, Problem?, wat, countdown, topic, set_topic, bs, offended, shed, sudo, python, sfd, source, link, chimay, psa, psa_backup, it_crowd, notes, note_to_self, software')
        return
    if ctx.message.content.lower().split()[1] == 'go' or ctx.message.content.lower().split()[1] == 'python' or ctx.message.content.lower().split()[1] == 'c' and len(ctx.message.content) > 1:
        await ctx.channel.send(f'curl https://cht.sh/{ctx.message.content.split()[1]}/{ctx.message.content.split()[2]}')
        return
    if ctx.message.content.lower().split()[1] == 'convert':
        await ctx.channel.send(f'{ctx.author.name} the syntax for convert: #convert <deg> <F|C>')
    if ctx.message.content.lower().split()[1] == 'rps':
        await ctx.channel.send(f'{ctx.author.name} the syntax for rps: #rps <rock|paper|scissors>')
    if ctx.message.content.lower().split()[1] == 'set_topic':
        await ctx.channel.send(f'the syntax for set_topic: #set_topic <topic>')
    if ctx.message.content.lower().split()[1] == 'note_to_self':
        await ctx.channel.send(f'the syntax for note_to_self: #note_to_self <notes>')
    if ctx.message.content.lower().split()[1] == 'ttt':
        await ctx.channel.send('ttt: Tic-Tac-Toe requires 2 players then synatx is #ttt x,y : you can always see the board with #ttt board')

@bot.command()
async def problem(ctx):
    await ctx.channel.send("Problem? https://clips.twitch.tv/ArtisticBlatantDogeCclamChamp")


@bot.command()
async def steamdeck(ctx):
    await ctx.channel.send(f'Hey @MitchsWorkshop, did you get a Steam Deck yet? No? Go get one then.')

@bot.command()
async def fake(ctx):
    await ctx.channel.send("@MitchsWorkshop is lying the sips are FAKE!")

@bot.command()
async def CONTENT(ctx):
    await ctx.channel.send("@MitchsWorkshop it is CONTENT TIME!")

@bot.command()
async def lcg(ctx):
    await ctx.channel.send('we all know mitch is a lonely cat guy')

@bot.command()
async def CONTENT_TIME(ctx):
    await ctx.channel.send("@MitchsWorkshop it is CONTENT TIME!")

@bot.command()
async def florida(ctx):
    await ctx.channel.send(f"{Florida_Man().return_it()}")

@bot.command()
async def rank(ctx):
    if ctx.channel.name == 'mitchsworkshop':
        await ctx.channel.send('1. krazynez_2 2. flyboy1565 . If your not first your last')

@bot.command()
async def ifconfig(ctx):
    await ctx.channel.send('My public IP is: 127.0.0.1 go ahead and DDOS me Kappa')

@bot.command()
async def Bot(ctx):
    await ctx.channel.send(f"Bot? I'm not a bot your a bot {ctx.author.name}!")

@bot.command()
async def import_(ctx):
    if len(ctx.message.content.split()) < 2:
        await ctx.channel.send("ModuleNotFoundError: No module named 'django;")
    else:
        await ctx.channel.send(f"ModuleNotFoundError: No module named '{ctx.message.content.split()[1]}'")


@bot.command()
async def ddg(ctx):
    if len(ctx.message.content.split()) < 2:
        return
    url = ctx.message.content.replace('#ddg ', '').replace(' ', '+')
    await ctx.channel.send(f'https://duckduckgo.com/?q={url}')

@bot.command()
async def man(ctx):
    if ctx.channel.name == 'mitchsworkshop' and len(ctx.message.content.split()) > 1:
        await ctx.channel.send(f"http://manpages.org/{ctx.message.content.split()[1]}")
    else:
        await ctx.channel.send(f"No manual entry for {ctx.message.content.split()[1]}")

    


@bot.command()
async def laptop(ctx):
    if ctx.channel.name.lower() == 'lastmiles':
        await ctx.channel.send('For fuck sakes it is a Lenovo X270 people.')

@bot.command()
async def ban(ctx):
    if len(ctx.message.content.split()) > 1:
        await ctx.channel.send(f'{ctx.message.content.split()[1]} has been banned.')

@bot.command()
async def define(ctx):
    print(len(ctx.message.content.split()))
    if len(ctx.message.content.split()) > 1:
        await ctx.channel.send(f'#define {ctx.message.content.split()[1]}')
    else:
        await ctx.channel.send('#define SIGBEER')
@bot.command()
async def bubblegum(ctx):
    if len(ctx.message.content.split()) > 2:
        await ctx.channel.send(f"I have come to chew {ctx.message.content.split()[1]} and kick {''.join(ctx.message.content.split()[2:])}... but I'm all of of {ctx.message.content.split()[1]}")
    else:
        await ctx.channel.send("I have come to chew bubblegum and kick ass... but I'm all of of bubblegum")

@bot.command()
async def django(ctx):
    if ctx.author.name == 'flyboy1565':
        replies = [f'Yes we know you like django {ctx.author.name}', f'For the love of God Mitch listen to {ctx.author.name}', f'Today we shall finally learn Django thanks to flyboy1565, if Mitch says "no" he is lying Kappa']
        random.seed()
        random.shuffle(replies)
        await ctx.channel.send(random.choice(replies))
    else:
        replies = [f'Whoa! You like Django too? Nice.', 'You and Flyboy can be friends', 'https://www.imdb.com/title/tt1853728/']
        random.seed()
        random.shuffle(replies)
        await ctx.channel.send(f'{ctx.author.name}: {random.choice(replies)}')

@bot.command()
async def django2(ctx):
    await ctx.channel.send('1, 2 Django\'s coming for you. 3, 4 flyboy\'s at your door. 5, 6 Django in the mix. 7, 8 gonna stay up late, 9, 10 you will learn Django again.')

@bot.command()
async def randomName(ctx):
    await ctx.channel.send(f'Random Name: {names.get_full_name()}')

@bot.command()
async def leopardName(ctx):
    await ctx.channel.send(f'The Leopard\'s name is Kumo.')

@bot.command()
async def anders(ctx):
    await ctx.channel.send('B A D B O T')


first = True
@bot.command()
async def count(ctx):
    global first
    if first:
        first = False
        name = ctx.channel.name
        await ctx.channel.send(f"{ctx.channel.name} your current viewer count is : {Twitch_Viewer_Count().get_count(name)}")
        first = True
    else:
        await ctx.channel.send(f'Not doxxing me today {ctx.author.name}! Nice Try :-p')

@bot.command()
async def power(ctx):
    await ctx.channel.send("I've got the powaaaa!!!")

@bot.command()
async def willItWork(ctx):
    random.seed()
    await ctx.channel.send(f'Chances of this actually working: {random.random() * 100}%')

@bot.command()
async def weather(ctx):
    import python_weather, asyncio
    if len(ctx.message.content.split()) < 2:
        return
    if len(ctx.message.content.split()) > 2 and ctx.message.content.split()[2].lower() == 'f':
        client = python_weather.Client(format=python_weather.IMPERIAL)
    else:
        client = python_weather.Client()
    w = await client.find(' '.join(ctx.message.content.split()[1:]))
    await client.close()
    if len(ctx.message.content.split()) > 2 and ctx.message.content.split()[2].lower() == 'f':
        # Pain in the ass to remove the f
        #await ctx.channel.send(f"Weather in {' '.join(ctx.message.content.split()[1:])}: {w.current.temperature}°F right now")
        return
    else:
        await ctx.channel.send(f"Weather in {' '.join(ctx.message.content.split()[1:])}: {w.current.temperature}°C right now")

@bot.command()
async def remember(ctx):
    resp = ["Remember when Mitch showed us his keys.... ahhh     good times.... good times...", "Remember when we went on a cup tangant.... good times...", "Mitch drinks from a CUP not a Pitcher Fight me! LUL", "Remember when Mitch was just a wee little streamer and knew nothing.... Well he still doesn't know much... Good times..."]
    random.seed()
    result = random.choice(resp)
    await ctx.channel.send(f'{result}')

@bot.command()
async def keys(ctx):
    with open('./db/keys.txt') as f:
        keys = f.read()
        f.close()
    await ctx.channel.send(f'Mitch has show his keys {keys.strip()} times.... so far...')

@bot.command()
async def google(ctx):
    await ctx.channel.send('No no no no, we use DuckDuckGo around here.')

@bot.command()
async def date(ctx):
    await ctx.channel.send(f"Date: {datetime.now().month}-{datetime.now().day}-{datetime.now().year} : {datetime.now().strftime('%H:%M:%S')}")

@bot.command()
async def fr(ctx):
    if len(ctx.message.content.split()) < 2:
        return
    else:
        await ctx.channel.send(f'{ctx.author.name} your request > /dev/null')

@bot.command()
async def riceCakes(ctx):
    await ctx.channel.send(f"@MitchsWorkshop you own @Krazynez_2 rice cakes, you need to pay up!")

@bot.command()
async def unhappy(ctx):
    await ctx.channel.send('Unhappy? Good your in the right place then.')

@bot.command()
async def baby(ctx):
    if len(ctx.message.content.split()) < 2:
        await ctx.channel.send('Hit me baby one more time!')
    else:
        await ctx.channel.send(f'{" ".join(ctx.message.content.split()[1:])} me baby one more time!')

@bot.command()
async def downUp(ctx):
    import os
    count = 0
    if os.path.isfile('./db/docker.txt'):
        with open('./db/docker.txt', 'r') as f:
            count = f.read()
            f.close()
            with open('./db/docker.txt', 'w') as f:
                count = int(count) + 1
                f.write(str(count))
                f.close()
        await ctx.channel.send(f"Mitch has Down Up'd {count} time(s)")
    else:
        with open('./db/docker.txt', 'w') as f:
            f.write('1')
            count = count + 1
            f.close()
        await ctx.channel.send(f"Mitch has Down Up'd {count} times")

@bot.command()
async def commands(ctx):
    await ctx.channel.send("ping, psa, psa_backup, it_crowd, note_to_self, notes, beer, paste, beertime, chimay, payme, stapler, canadian, rgb, bedtime, timer, link, distract, add_distraction, os, nvidia, sh, discord, vim, judged, caps, vscode, python, sfd, src, shed, sudo, offended, convert, wwad, set_topic, topic, countdown, wat, linux, bs, whiskeyVsScotch, software, jebaited, help, problem, fake, CONTENT_TIME, florida, rank, man, laptop, ban, define, bubblegum, django, rps, ttt, (More not listed)" )

@bot.command()
async def rps(ctx):
    guess=random.choice(['rock', 'paper', 'scissors'])

    if ctx.message.content.lower().split()[1]== 'rock' and guess == 'scissors':
        await ctx.channel.send(f'{ctx.author.name} you win!!! I chose {guess}')
    elif ctx.message.content.lower().split()[1] == 'rock' and guess == 'paper':
        await ctx.channel.send(f'{ctx.author.name} you loose!! I chose {guess}')
    elif ctx.message.content.lower().split()[1]  == 'paper' and guess == 'scissors':
        await ctx.channel.send(f'{ctx.author.name} you loose!! I chose {guess}')
    elif ctx.message.content.lower().split()[1]  == 'paper' and guess == 'rock':
        await ctx.channel.send(f'{ctx.author.name} you win!!! I chose {guess}')
    elif ctx.message.content.lower().split()[1]  == 'scissors' and guess == 'rock':
        await ctx.channel.send(f'{ctx.author.name} you loose!! I chose {guess}')
    elif ctx.message.content.lower().split()[1]  == 'scissors' and guess == 'paper':
        await ctx.channel.send(f'{ctx.author.name} you win!!! I chose {guess}')
    else:
        await ctx.channel.send(f'{ctx.author.name} looks like we tied!')

initial = True
player = 0 
game = TickTacToe()
people = []
first_run = True

@bot.command()
async def ttt(ctx):
    global initial
    global first_run
    global game
    global people
    global player
    global playerTwo, playerOne
    global playerOneTurn, playerTwoTurn
    if initial is True:
        initial = False
        people.append(ctx.author.name)
        await ctx.channel.send(f"{ctx.author.name} is {game.setup(ctx.author.name)}")
        return
    if len(ctx.message.content.lower().split()) == 1:
        people.append(ctx.author.name)
        await ctx.channel.send(f"{ctx.author.name} is {game.setup(ctx.author.name)}")
        return
    elif len(ctx.message.content.lower().split()) == 2 and ctx.message.content.lower().split()[1] == 'board':
        for i in range(0, 3):
            await ctx.channel.send(game.board(i))
            time.sleep(2)
    elif len(ctx.message.content.lower().split()) == 2 and ctx.message.content.lower().split()[1] == 'layout':
        for i in range(0, 3):
            await ctx.channel.send(game.board_layout(i))
            time.sleep(2)
    elif len(ctx.message.content.split()) == 2 and ctx.message.content.lower().split()[1] == 'reset':
        del game
        people = []
        initial = True
        player = 0
        await ctx.channel.send('Game has been reset')
        game = TickTacToe()
        return
    elif len(ctx.message.content.lower().split()) == 2:
        playerOne = people[0]
        playerTwo = people[1]
        if player % 2 == 0:
            playerOneTurn =True
            playerTwoTurn = False
            #type(game.counter())
            #player = game.counter() 
        else:
            playerOneTurn = False
            playerTwoTurn = True
            #player = game.counter()
    if ctx.author.name in playerTwo and playerOneTurn and ctx.message.content.lower().split()[2] != 'board':
         await ctx.channel.send(f"Not your turn: it is {playerOne}'s turn.")
         return
    elif ctx.author.name in playerOne and playerTwoTurn and ctx.message.content.lower().split()[2] != 'board':
         await ctx.channel.send(f"Not your turn: it is {playerTwo}'s turn.")
         return
    else:
         if player == 1:
            #player = game.counter()
            player = 0
         else:
            player = 1 #game.counter()
    if not ctx.author.name in people:
        await ctx.channel.send('Your not in this game!')
        return
    values = ctx.message.content.split()[1]
    values = values.split(',')
    await ctx.channel.send(game.play(values[0], values[1]))
    return

