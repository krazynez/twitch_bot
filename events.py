#!/usr/bin/env python3

from comms import bot
from datetime import datetime, timedelta
import random

# Runs if user uses prefix that does not exist
@bot.event()
async def event_command_error(ctx, error):
    if ctx.content.lower() == '#include':
        return
    if ctx.channel.name != 'mitchsworkshop':
        print(error)
        #await ctx.channel.send(f"Hey @{ctx.author.name} stop trying to break me!")
        return

@bot.event()
async def event_usernotice_subscription(ctx):
    if ctx.channel.name == 'lastmiles':	
    	await ctx.channel.send('Whoooo hooooo Beer Money!!!')
    elif ctx.channel.name == 'mitchsworkshop':
        response = ['Welcome to the Big Brain club', 'Welcome, don\'t forget to give Mitch a hard time', 'Welcome and don\'t forget to join our discord! (!discord)', '@MitchsWorkshop, this person has joined the Big Brain community!']
        random.seed()
        random.shuffle(response)
        await ctx.channel.send(random.choice(response))

    # Checks chat to see if any of the user input is listed
@bot.event()
async def event_message(ctx):
    #await bot.handle_commands(ctx)
    if "krazybot_2" in ctx.content.lower() and 'ttt' not in ctx.content.lower():
        replies = [f'The correct answer {ctx.author.name} is ....... 42', f"{ctx.author.name} leave me alone, I don't want to talk to you.",
                  f"{ctx.author.name} your mom",
                  f"{ctx.author.name} sometimes",
                  f"{ctx.author.name} no",
                  f"{ctx.author.name} yeah I guess so",
                  ]
        random.seed()
        random.shuffle(replies) 
        await ctx.channel.send(random.choice(replies))

    if "want to become famous" in ctx.content.lower():
        await ctx.channel.send(f"{ctx.author.name} should be banned")

    # Used for rwxrob stream (mainly)
    if ctx.channel.name == 'rwxrob':
        if not os.path.isfile('./db/doNotAt.txt'):
            open('./db/doNotAt.txt', 'a').close()
        with open('./db/doNotAt.txt', 'r') as f:
            doNotAt = f.readlines()
            f.close()
        if ctx.channel.name == 'rwxrob' and ctx.author.name + '\n' not in doNotAt and ctx.author.name != 'krazybot_2' and ctx.author.name != 'rwxrob' and not ctx.author.is_mod:
            with open('./db/doNotAt.txt', 'a') as f:
                if f in doNotAt:
                    f.close()
                else:
                    f.write(f'{ctx.author.name}')
                    f.write('\n')
                    f.close()
            await ctx.channel.send(f'{ctx.author.name} Go here to chat: chat.rwx.gg or on freenode ##rwx.gg')
# Need to ask if this is okay still
#    if ctx.channel.name == 'rwxrob':
#        await ctx.channel.send(f'Go to chat.rwx.gg to chat live. Or you can go to ##rwx.gg on freenode IRC. (Twitch Chat is mostly ignored.)')

# People use the word 'python' a lot
    #if 'python' in ctx.content.lower():
    #    await ctx.channel.send(f"Shhhh, @{ctx.author.name} don't tell them my secret..")
    if '2038' in ctx.content.lower():
        now = datetime.now()
        ft = datetime(2038, 1, 19, 2, 14, 7)
        _time = ft - now
        await ctx.channel.send(f'Time left until Y2K38 --> Days: {_time.days} Seconds: {_time.seconds}')
    elif 'what movie is' in ctx.content.lower() or 'movie name' in ctx.content.lower() or 'name of movie' in ctx.content.lower() or 'what film' in ctx.content.lower() or 'what\'s the title' in ctx.content.lower() or 'what is the name of this movie' in ctx.content.lower() or 'what r we watching' in ctx.content.lower():
        await ctx.channel.send(f'@{ctx.author.name} this movie is: Driving Miss Daisy, and the next on the line up: Driving Miss Daisy')
    elif 'esther' in ctx.content.lower():
        await ctx.channel.send(f"@{ctx.author.name} shhhh we don't speak of that word")

