from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
import time 

class Twitch_Viewer_Count():
    def __init__(self):
        self.options = Options()
        self.options.add_argument('--headless')
        self.driver = webdriver.Firefox(options=self.options)
        #self.driver = webdriver.Firefox()

    def close_connection(self):
        self.driver.close()

    def get_count(self, channel) -> str:
        self.url = f"https://twitch.tv/{channel}"
        # Testing
        #self.url = f"https://twitch.tv/mitchsworkshop"
        self.driver.get(self.url)
       
        time.sleep(15)

        #result = self.driver.find_element(By.CLASS_NAME, "ScAnimatedNumber-sc-acnd2k-0").text
        result = self.driver.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/div[1]/main/div[2]/div[3]/div/div/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div/p/span').text
        self.close_connection() 
        return result
